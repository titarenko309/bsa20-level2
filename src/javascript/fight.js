import {createElement} from './helpers/domHelper';
import {showModal} from './modals/modal';
import {createFighterDetails} from './modals/fighterDetails';

var firstFighterHealthElement= null;
var secondFighterHealthElement = null;

export async function fight(firstFighter, secondFighter) {
    // return winner

    var firstFighterHealth = firstFighter.health;
    var secondFighterHealth = secondFighter.health;
    var damage;

    showFightingRingModal(firstFighter, secondFighter);

    while (firstFighterHealth > 0 || secondFighterHealth > 0) {

        damage = getDamage(firstFighter, secondFighter);
        await sleep(500);

        firstFighterHealth = firstFighterHealth - damage;
        if (firstFighterHealth <= 0) {
            firstFighterHealthElement.innerText = 'Health: ' + 0;
            break;
        }
        firstFighterHealthElement.innerText = 'Health: ' + Math.ceil(firstFighterHealth);

        damage = getDamage(firstFighter, secondFighter);
        await sleep(500);

        secondFighterHealth = secondFighterHealth - damage;

        if (secondFighterHealth <= 0) {
            secondFighterHealthElement.innerText = 'Health: ' + 0;
            break;
        }
        secondFighterHealthElement.innerText = 'Health: ' + Math.ceil(secondFighterHealth);
    }

    if (firstFighterHealth > secondFighterHealth) {
        return firstFighter;
    } else {
        return secondFighter;
    }
}

export function getDamage(attacker, enemy) {
    // damage = hit - block
    // return damage
    var damage = getHitPower(attacker) - getBlockPower(enemy);
    if (damage < 0) {
        return 0;
    } else {
        return damage;
    }
}

export function getHitPower(fighter) {
    // return hit power
    var criticalHitChance = Math.random() + 1;
    var hitPower = fighter.attack * criticalHitChance;
    return hitPower;
}

export function getBlockPower(fighter) {
    // return block power
    var dodgeChance = Math.random() + 1;
    var blockPower = fighter.defense * dodgeChance;
    return blockPower;
}

export function showFightingRingModal(firstFighter, secondFighter) {
    const title = 'Fight!';
    const bodyElement = addFightersToRing(firstFighter, secondFighter);
    showModal({title, bodyElement});
}

function addFightersToRing(firstFighter, secondFighter) {
    var firstFighterDetails = createFighterDetails(firstFighter);
    var secondFighterDetails = createFighterDetails(secondFighter);
    firstFighterHealthElement = firstFighterDetails.getElementsByClassName('fighter-health')[0];
    secondFighterHealthElement = secondFighterDetails.getElementsByClassName('fighter-health')[0];
    secondFighterDetails.getElementsByClassName('fighter-image')[0].style.transform = 'scaleX(-1)';

    var attributes = {style: "display: flex"};
    const fightingRing = createElement({tagName: 'div', className: 'modal-body', attributes});

    fightingRing.append(firstFighterDetails, secondFighterDetails);

    return fightingRing;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}