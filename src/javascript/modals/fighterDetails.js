import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  var attributes = { align: "center" };
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'div', className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'div', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'div', className: 'fighter-defense' });

  attributes = { src: source };
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = 'Name: ' + name;
  healthElement.innerText = 'Health: ' + health;
  attackElement.innerText = 'Attack: ' + attack;
  defenseElement.innerText = 'Defence: ' + defense;

  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}
