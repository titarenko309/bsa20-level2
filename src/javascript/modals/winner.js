import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  // show winner name and image
    const title = 'Winner!';
    fighter.then(fighter => {const bodyElement = createWinnerDetails(fighter);
        showModal({ title, bodyElement });
    });
}

function createWinnerDetails(fighter) {
    const { name, source } = fighter;

    var attributes = { align: "center" };
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes });
    const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
    attributes = { src: source };
    const imgElement = createElement({ tagName: 'img', className: 'fighter-image-mirror', attributes });

    nameElement.innerText = name;

    fighterDetails.append(nameElement, imgElement);

    return fighterDetails;
}